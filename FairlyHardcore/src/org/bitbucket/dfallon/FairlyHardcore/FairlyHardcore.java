package org.bitbucket.dfallon.FairlyHardcore;

import java.util.Arrays;
import java.util.logging.Level;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;	
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

public class FairlyHardcore extends JavaPlugin {
	
	public static Items items;
	public static FileConfiguration config;
	public static JavaPlugin plugin;
	
	@Override
	public final void onEnable(){
		items = new Items(this, "TrackedItems.yml");
		config = this.getConfig();
		plugin = this;
		this.getLogger().log(Level.INFO,"config loaded!");
		getServer().getPluginManager().registerEvents(new Handlers(), this);
		
	}
	
	@Override
	public final void onDisable(){
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if(cmd.getName().equalsIgnoreCase("FairlyHardcore")){
			//check for args
			if(args.length<1){
				sender.sendMessage("more arguments required");
				return false;
			}
			//respond to remove command
			if(args[0].equalsIgnoreCase("remove")){
				if(sender instanceof Player){
					long offset = 30000; //30 seconds
					//store time in future to expire clickable action
					FixedMetadataValue m = new FixedMetadataValue(this, System.currentTimeMillis()+offset);
					((Player) sender).setMetadata("FairlyHardcore.remove.select", m);
					sender.sendMessage("Select a block by clicking on it");
					return true;
				}else if(args[0].equalsIgnoreCase("config")&&args.length>3){
					String value = "";
					for(String arg:Arrays.copyOfRange(args,2,args.length))
							value = value+((value.equals(""))?"":" ")+arg;
					config.set(args[0], value);
				}else{
					sender.sendMessage("You must be a player");
					return false;
				}
			}
		}
		return false;
	}

}
