package org.bitbucket.dfallon.FairlyHardcore;

import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

public class ExplosionRunnable extends BukkitRunnable {
	private Location location;
	
	public Location getLocation() {
		return location.clone();
	}
	public void setLocation(Location location) {
		
		this.location = location;
	}
	public ExplosionRunnable(Location l){
		location = l;
	}
	@Override
	public void run() {
		if(location.getBlock().isEmpty())
			return;
		location.getWorld().createExplosion(location, 4);
	}

}
