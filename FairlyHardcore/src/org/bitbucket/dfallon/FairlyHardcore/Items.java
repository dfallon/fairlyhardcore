package org.bitbucket.dfallon.FairlyHardcore;

import java.util.Collection;
import java.util.HashSet;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class Items {
	
	private ConfigAccessor data;
	private final JavaPlugin plugin;
	
	//constructor
	public Items(JavaPlugin p, String fileName){
		plugin = p;
		data = new ConfigAccessor(plugin,fileName);
	}
	
	public static Block getBlock(String location){
		String[] tokens = location.split("/"); 
		int x = Integer.parseInt(tokens[1]);
		int y = Integer.parseInt(tokens[2]);
		int z = Integer.parseInt(tokens[3]);
		return FairlyHardcore.plugin.getServer().getWorld(tokens[0]).getBlockAt(x,y,z);
	}
	public static String getBlock(Block b){
		String world = b.getWorld().getName();
		String x = Integer.toString(b.getX());
		String y = Integer.toString(b.getY());
		String z = Integer.toString(b.getZ());
		return world+"/"+x+"/"+y+"/"+z;
	}
	public void addItem(Block b,Player p){
		data.getConfig().set(getBlock(b), p.getName());
		data.saveConfig();
	}
	
	public void removeItem(Block b){
		data.getConfig().set(getBlock(b), null);
		data.saveConfig();
	}
	
	private void destroyItem(Block b,long delay){
		if(plugin.getConfig().getBoolean("Explosions.enabled"))
			scheduleExplosion(b,delay);
		removeItem(b);
	}
	
	private void scheduleExplosion(Block b, long delay) {
		BukkitTask explosion = new ExplosionRunnable(b.getLocation()).runTaskLater(plugin, delay); 
		
	}

	public void destroyItems(Player p){
		Collection<Block> items = getItemList(p);
		int i = 0;
		for(Block item:items){
			destroyItem(item, plugin.getConfig().getLong("Explosions.delay")*i);
			i++;
			
		}
	}

	private Collection<Block> getItemList(Player p) {
		Collection<String> allBlocks = data.getConfig().getKeys(false);
		Collection<Block> blocks = new HashSet<Block>();
		//add blocks that match
		for(String block: allBlocks){
			if(p.getName().equalsIgnoreCase(data.getConfig().getString(block)))
				blocks.add(getBlock(block));
		}
		return blocks;
		
	}

}
