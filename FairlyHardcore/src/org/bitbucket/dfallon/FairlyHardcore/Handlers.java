package org.bitbucket.dfallon.FairlyHardcore;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.MetadataValue;


public class Handlers implements Listener {
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		if(e.getEntity() instanceof Player){
			Player player = (Player) e.getEntity();
			player.getInventory().clear();
			player.getEnderChest().clear();
			FairlyHardcore.items.destroyItems(player);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(e.getPlayer().getGameMode().equals(GameMode.CREATIVE)&&FairlyHardcore.config.getBoolean("TrackCreative"))
			if(FairlyHardcore.config.getList("Blocks").contains(e.getBlock().getType().getId()))
				FairlyHardcore.items.addItem(e.getBlock(), e.getPlayer());
		
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(FairlyHardcore.config.getList("Blocks").contains(e.getBlock().getType().getId()))
			FairlyHardcore.items.removeItem(e.getBlock());
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		if(!e.getAction().equals(Action.LEFT_CLICK_BLOCK))
			return;
		List<MetadataValue> select = e.getPlayer().getMetadata("FairlyHardcore.remove.select");
		if(select.size()==0)
			return;
		MetadataValue commandTime = select.get(select.size()-1);
		if((commandTime!=null)&&(commandTime.asLong()>System.currentTimeMillis())){
			FairlyHardcore.items.removeItem(e.getClickedBlock());
			e.getPlayer().removeMetadata("FairlyHardcore.remove.selct", FairlyHardcore.plugin);
			e.getPlayer().sendMessage("Block has been removed from tracking");
		}
	}
	
}
